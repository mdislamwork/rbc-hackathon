from .logic_adapter import LogicAdapter
from chatterbot.adapters.logic import LogicAdapter
from chatterbot.conversation import Statement
from textblob.classifiers import NaiveBayesClassifier
import nltk

nltk.data.path.append('./nltk_data/')

training_lists = {
    'operating_revenue' : [
        "what is the operating income of",
                "operating income",
        "what were the operating income of",
                "were the operating income",
        "what is the operating income for",
                "operating income for",
        "what were the operating income for",
                "operating revenue for",
        "what is the operating revenue of",
                "operating revenue of",
        "what is the operating revenue for",
                "operating revenue for",
        "what is the consolidated operating income of",
                "consolidated operating income of",
        "what is the comprehensive operating income of",
                "comprehensive operating income of",
        "what is the sales generated of",
        "top sales generated",
        "what company has the least cost of operating",
        "what company has the least operating revenue"
    ],
    'company_rank' : [
        "what are the top companies",
                "top companies",
        "who are the top companies",
                "the top companies",
        "companies with negative income",
                "negative income",
        "how high is",
        "who is lask ranked",
        "how prestigious is",
        "who is ranked number one",
        "who is ranked second",
        "what company is the most fun to work at",
        "what company is the least stress",
        "what company is the most famous",
        "who is famous",
        "what company is hated",
        "what company ranks most in pay",
        "what company gives most pay",
        "what company gives least pay",
        "what company should I work for"
    ]
}

master_training_list = [];
[master_training_list.extend(i) for i in training_lists.values() ]

class HackLogicAdapters(LogicAdapter):

    def __init__(self, **kwargs):
        super(LogicAdapter, self).__init__(**kwargs)

    def assembleTrainingList(self):
        self.training_data.extend( \
        [ (i, 0) for i in self.master_question_list ] )

    def process(self, statement):
        confidence = self.classifier.classify(statement.text.lower());
        response = Statement("{0}. Selection Confidence: {1}".format(self.basic_statement, confidence) )

        return confidence, response

    # TODO: Include phrases for other adapters here.
    master_question_list = [
        "what is the operating income of",
        "what is the operating revenue of",
        "what is the consolidated operating income of",
        "what is the comprehensive operating income of",
        "what are the top companies",
        "who are the top companies",
        "companies with negative income",
        # BS Questions
        "who are you?",
        "what do you like to do?",
        "do you have a purpose in life?",
        "what would you be doing if you weren't a chatbot?",
        "did you ever dream of a dream?",
        "when the sound of guns doesn't",
    ]


class OperatingRevenueLogicAdapter(HackLogicAdapters):

    def __init__(self, **kwargs):
        super(HackLogicAdapters, self).__init__(**kwargs)

        # TODO: Include phrases for other adapters here.
        self.training_data = [
            ("what is the operating income of", 1),
            ("what were the operating income of", 1),
            ("what is the operating income for", 1),
            ("what were the operating income for", 1),
            ("what is the operating revenue of", 1),
            ("what is the operating revenue for", 1),
            ("what is the consolidated operating income of", 1),
            ("what is the comprehensive operating income of", 1),
        ]
        self.assembleTrainingList()
        self.classifier = NaiveBayesClassifier(self.training_data)
        self.basic_statement = \
            "This is a question about operating incomes."

    def process(self, statement):
        confidence = self.classifier.classify(statement.text.lower());
        response = Statement( \
            "Main Response: {0}\nConfidence: {1}\nTokenization:{2}" \
            .format(self.basic_statement + str(master_training_list), confidence, \
                nltk.pos_tag( nltk.word_tokenize( str( statement) ) ) ) )

        return confidence, response

class CompanyRankLogicAdapter(HackLogicAdapters):

    def __init__(self, **kwargs):
        super(HackLogicAdapters, self).__init__(**kwargs)
        # TODO: Improve training data.
        self.training_data = [
            ("what are the top companies", 1),
            ("who are the top companies", 1),
            ("companies with negative income", 1),
        ]

        self.assembleTrainingList()
        self.classifier = NaiveBayesClassifier(self.training_data)
        self.basic_statement = "This is a statement about company rankings."


# TODO: Make a master list of questions for all of these classes.
# TODO: Have questions specified in a class declare '1' for those
# questions and 0 for all others.
# MYCROFT AI, adapt intent parsert.

#TODO: Implement the following questions
# - What is a total revenue? 
# - What is a gross profit?
# - What is a cost of revenue?
# - What is a lowest operating expenses?
# - What is a top operating expenses?
# - What is a highest negative income?
# - What is a net income? 


# Basic process statement.
#def process(self, statement):
    #confidence = self.classifier.classify(statement.text.lower());
    #response = Statement("This is a question about operating incomes. With confidence {0}".format(confidence) )
#
    #return confidence, response
#
